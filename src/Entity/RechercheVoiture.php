<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class RechercheVoiture{

    /**
     * @Assert\LessThanOrEqual(propertyPath="maxAnnee", message="doit être plus petit que l'année Max")
     */
    private $minAnnee;
    /**
     * @Assert\GreaterThanOrEqual(propertyPath="minAnnee", message="doit être plus grand que l'année Min")
     */
    private $maxAnnee;

    /**
     * @return mixed
     */
    public function getMinAnnee()
    {
        return $this->minAnnee;
    }

    /**
     * @return mixed
     */
    public function getMaxAnnee()
    {
        return $this->maxAnnee;
    }

    /**
     * @param mixed $minAnnee
     */
    public function setMinAnnee(int $annee)
    {
        $this->minAnnee = $annee;
        return $this;
    }

    /**
     * @param mixed $maxAnnee
     */
    public function setMaxAnnee(int $annee)
    {
        $this->maxAnnee = $annee;
        return $this;
    }
}